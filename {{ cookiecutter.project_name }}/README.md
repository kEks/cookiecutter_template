{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

Project Organization
------------


    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- Manuel or automatic Results and Discussion
    │   └── reports        <- Generated analysis as HTML, PDF, LaTeX, etc. And its interpretation
    │
    ├── enviroment         <- Conda enviroment and container specification files
    │
    ├── src                <- Source code for use in this project.
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   ├── explore        <- Scripts/Notebooks to explore and understand the data
    │   ├── pipeline       <- snakemake pipeline to systematicly analyse the data
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │
    └── .gitignore         <- ignore the files not needed/wanted in git repo


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
